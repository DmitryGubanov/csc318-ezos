package csc318.ezos;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;


public class Home extends ActionBarActivity {

    private Calendar cClock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN |
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);

        setContentView(R.layout.activity_home);

        getSupportActionBar().hide();

        cClock = Calendar.getInstance();
        setTime();

        decorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        // Note that system bars will only be "visible" if none of the
                        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                            View decorView = getWindow().getDecorView();
                            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN |
                                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                            decorView.setSystemUiVisibility(uiOptions);
                            getSupportActionBar().hide();
                        }

                        setTime();
                    }
                });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void goToPhoneApp(View view) {
        Intent intent = new Intent(this, phone_contacts.class);
        startActivity(intent);

    }

    public ProgressDialog pd;

    public void goToCameraApp(View view) {
        pd = ProgressDialog.show(this, "Launching camera", "Please wait for the camera to be ready...", true);
        PackageManager packageManager = getPackageManager();
        List<ApplicationInfo> list = packageManager.getInstalledApplications(PackageManager.GET_UNINSTALLED_PACKAGES);
        for (int n=0;n<list.size();n++) {
            if((list.get(n).flags & ApplicationInfo.FLAG_SYSTEM)==1)
            {
                if(list.get(n).loadLabel(packageManager).toString().equalsIgnoreCase("Camera")) {
                    String defaultCameraPackage = list.get(n).packageName;
                    Intent cameraIntent = packageManager.getLaunchIntentForPackage(defaultCameraPackage);
                    startActivityForResult(cameraIntent, 123);
                    break;
                }
            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        if (requestCode == 123) {
            pd.cancel();
        }
    }

    public void goToTextingApp(View view) {
        Intent intent = new Intent(this, chat_contacts.class);
        startActivity(intent);

    }

    public void goToMoreApps(View view) {
        Intent intent = new Intent(this, app_drawer.class);
        startActivity(intent);

    }

    public void goToSearch(View view) {
        Intent intent = new Intent(this, search.class);
        startActivity(intent);

    }

    private void setTime() {
        TextView tvTime = (TextView) findViewById(R.id.home_navbar_time);

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm", Locale.US);
        String sTime = sdf.format(cClock.getTime());

        tvTime.setText(sTime);

    }
}
