package csc318.ezos;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


public class search extends ActionBarActivity {

    private InputMethodManager imm;

    private Calendar cClock;

    private ArrayList<String> alApps;

    private ArrayList<String> alDesc;

    private ArrayList<String[]> alTags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        View decorView = getWindow().getDecorView();
        // Hide the status bar.
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN |
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);

        setContentView(R.layout.activity_search);

        getSupportActionBar().hide();

        cClock = Calendar.getInstance();
        setTime();

        decorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        // Note that system bars will only be "visible" if none of the
                        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                            View decorView = getWindow().getDecorView();
                            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN |
                                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                            decorView.setSystemUiVisibility(uiOptions);
                            getSupportActionBar().hide();
                        }

                        setTime();
                    }
                });

        // this is going to be long and tedious, but I'm too lazy to do it better
        alApps = new ArrayList<String>();
        alDesc = new ArrayList<String>();
        alTags = new ArrayList<String[]>();

        alApps.add(0, "> Wi-Fi Settings");
        alDesc.add(0, "connect to a Wi-Fi network or modify Wi-Fi settings.");
        alTags.add(0, new String[]{"wifi", "wi-fi", "settings"});

        alApps.add(1, "> Solitaire");
        alDesc.add(1, "play a game of solitaire");
        alTags.add(1, new String[]{"game", "games", "solitaire"});

        alApps.add(2, "> Tic Tac Toe");
        alDesc.add(2, "play a game of Tic Tac Toe");
        alTags.add(2, new String[]{"game", "games", "tictactoe", "tic", "tac", "toe"});
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void goToHome(View view) {
        finish();

    }

    public void searchPhone(View view) {
        imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);

        String searchTerm;
        searchTerm = ((EditText) findViewById(R.id.search_searchedittext)).getText().toString();

        LinearLayout searchResults = (LinearLayout) findViewById(R.id.search_searchresults);
        searchResults.removeAllViews();

        for (int i = 0; i < 3; i++) {
            for (String tag : alTags.get(i)) {
                if (tag.equalsIgnoreCase(searchTerm)) {
                    LinearLayout ll = new LinearLayout(this);
                    ll.setOrientation(LinearLayout.VERTICAL);
                    ll.setPadding(16,16,16,0);

                    TextView tvApp = new TextView(this);
                    tvApp.setText(alApps.get(i));
                    tvApp.setTextSize(27);
                    TextView tvDesc = new TextView(this);
                    tvDesc.setText(alDesc.get(i));
                    tvDesc.setTextSize(17);
                    tvDesc.setTextColor(getResources().getColor(R.color.grey));

                    ll.addView(tvApp);
                    ll.addView(tvDesc);

                    searchResults.addView(ll);
                }
            }
        }

    }

    private void setTime() {
        TextView tvTime = (TextView) findViewById(R.id.search_navbar_time);
        System.out.println(tvTime);

        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm", Locale.US);
        String sTime = sdf.format(cClock.getTime());

        tvTime.setText(sTime);

    }
}
